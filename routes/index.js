 // GET home page.
 // 
 var crypto = require('crypto');
 var mongo = require('mongodb');
 var mongoUri = process.env.MONGOLAB_URI ||
 	process.env.MONGOHQ_URL ||
 	'mongodb://localhost/microblog';
 var User = require('../models/user.js');
 var Post = require('../models/post.js');
 var Posted = require('../models/posted.js');
 var Theme = require('../models/theme.js');
 var markdown = require("markdown-js");
 var url = require("url");
 var querystring = require('querystring');
 exports.index = function(req, res) {
 	var content = "";
 	var timestamp = "";
 	var username = "";
 	username = req.session.username;

 	if (content != "") {
 		content = markdown.makeHtml(req.body['txtcontent']);
 	}
 	res.render('index', {
 		title: '唐记2014',
 		postedit: '<color=\"red\">写</color>',
 		user: req.session.user,
 		seeboard: content,
 		success: req.flash('success').toString(),
 		error: req.flash('error').toString()
 	});

 };

 // Post.get(null, function(err, posts) {
 // if (err) {
 // 	posts = [];
 // }


 /*用户页面*/
 exports.user = function(req, res) {
 	User.get(req.params.user, function(err, user) {
 		if (!user) {
 			req.flash('error', '用户不存在');
 			return res.redirect('/');
 		}
 		Post.get(user.name, function(err, posts) {
 			if (err) {
 				req.flash('error', err);
 				return res.redirect('/');
 			}
 			res.render('user', {
 				title: user.name,
 				posts: posts,
 				user: req.session.user,
 				success: req.flash('success').toString(),
 				error: req.flash('error').toString()
 			});
 		});
 	});
 };

 //注册页面
 exports.reg = function(req, res) {
 	res.render('reg', {
 		title: '用户注册',
 		user: req.session.user,
 		success: req.flash('success').toString(),
 		error: req.flash('error').toString()
 	});
 };
 //点击注册按钮时触发的事件
 exports.doReg = function(req, res) {
 	//检查密码
 	if (req.body['password-repeat'] != req.body['password']) {
 		req.flash('error', '两次输入的密码不一致');
 		return res.redirect('/');
 	}

 	//生成md5的密码
 	var md5 = crypto.createHash('md5');
 	var password = md5.update(req.body.password).digest('base64');

 	var newUser = new User({
 		name: req.body.username,
 		password: password,
 	});

 	//检查用户名是否已经存在
 	User.get(newUser.name, function(err, user) {
 		if (user)
 			err = 'Username already exists.';
 		if (err) {
 			req.flash('error', err);
 			return res.redirect('/');
 		}
 		//如果不存在則新增用戶
 		newUser.save(function(err) {
 			if (err) {
 				req.flash('error', err);
 				return res.redirect('/');
 			}
 			req.session.user = newUser;
 			req.flash('success', '注册成功');
 			res.redirect('/');
 		});
 	});
 };

 ////用户登入页面
 exports.login = function(req, res) {
 	res.render('login', {
 		title: '用户登录',
 		user: req.session.user,
 		success: req.flash('success').toString(),
 		error: req.flash('error').toString()
 	});
 };
 ////点击登入时触发的事件
 exports.doLogin = function(req, res) {
 	//生成口令的散列值
 	var md5 = crypto.createHash('md5');
 	var password = md5.update(req.body.password).digest('base64');

 	User.get(req.body.username, function(err, user) {
 		if (!user) {
 			req.flash('error', '用户不存在');
 			return res.redirect('/');
 		}
 		if (user.password != password) {
 			req.flash('error', '密码错误');
 			return res.redirect('/');
 		}
 		req.session.user = user;
 		req.flash('success', '登录成功');
 		res.redirect('/');
 	});
 };
 ////点击登出时触发的事件
 exports.logout = function(req, res) {
 	req.session.user = null;
 	req.flash('success', '登出成功');
 	res.redirect('/');
 };


 exports.getArticals = function(req, res) {
 	var currentUser = req.session.user;
 	Post.get(currentUser.name, function(err, posts) {
 		if (err) {
 			posts = [];
 		}
 		posts.forEach(function(piece) {
 			if (piece.post != "" || piece.post != null) {
 				content += piece.user + "</br>" + markdown.makeHtml(piece.post) + "</br>" + piece.time.toString().slice(0, 25) + "</br>";
 			}
 		});
 		res.render('index', {
 			title: '唐记2014',
 			postedit: '<color=\"red\">写</color>',
 			user: req.session.user,
 			seeboard: content,
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 	});
 }



 exports.post = function(req, res) {
 	var currentUser = req.session.user;
 	var content = markdown.makeHtml(req.body['txtcontent']);
 	var articalname = "这是测试文章";
 	var post = new Post('test', currentUser.name, articalname, req.body['txtcontent']);
 	post.save(function(err) {
 		if (err) {
 			req.flash('error', err);
 			return res.redirect('/');
 		}
 		//res.redirect('/u/' + currentUser.name);


 		if (content != null || content != "") {
 			res.render('index', {
 				seeboard: content,
 				title: '唐记2014',
 				postedit: '<color=\"red\">写</color>',
 				user: req.session.user,
 				success: req.flash('success').toString(),
 				error: req.flash('error').toString()
 			});
 		}
 	});
 };

 exports.aboutus = function(req, res) {
 	res.render('about', {
 		title: 'TsherSteven'
 	});
 };

 exports.poststore = function(req, res) {

 	var content = markdown.makeHtml(req.body['txtcontent']);

 	if (content != null || content != "") {
 		res.render('index', {
 			seeboard: content,
 			title: '唐记2014',
 			postedit: '<color=\"red\">写</color>',
 			user: req.session.user,
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 	}
 };

 exports.showpostedit = function(req, res) {
 	var currentUser = req.session.user;
 	res.render('eblogs', {
 		title: '唐记2014',
 		user: currentUser,
 		edtitle: "",
 		isedit: "false",
 		edpostcontent: "",
 		ispreview: "false",
 		success: req.flash('success').toString(),
 		error: req.flash('error').toString()
 	})
 }
 exports.postArticals = function(req, res) {
 	var currentUser = req.session.user;
 	var classify = "测试";
 	var articalname = req.body['edtitle'];
 	var content = req.body['edpostcontent'];
 	var time = new Date();
 	var post = new Post(classify, currentUser.name, articalname, content, time);
 	post.save(function(err) {
 		if (err) {
 			req.flash('error', err);
 			return res.redirect('/');
 		}
 		//res.redirect('/u/' + currentUser.name);

 		if (content != null || content != "") {
 			res.render('blogs', {
 				classify: classify,
 				seeboard: markdown.makeHtml(content),
 				articalname: articalname,
 				time: time.toString().slice(0, 25),
 				title: '唐记2014',
 				user: req.session.user,
 				ispreview: "false",
 				success: req.flash('success').toString(),
 				error: req.flash('error').toString()
 			});
 		}
 	});
 }
 exports.postPreview = function(req, res) {
 	var currentUser = req.session.user;
 	var classify = "测试";
 	var articalname = req.body['edtitle'];
 	var content = req.body['edpostcontent'];
 	var time = new Date();
 	var postcode = req.session.postcode;
 	var post;
 	if (postcode) {
 		post = new Post(classify, currentUser.name, articalname, content, time, postcode); //编辑
 	} else {
 		post = new Post(classify, currentUser.name, articalname, content, time); //新建
 	}

 	var isedit = req.session.isedit;
 	if (post != null) {
 		req.session.post = post;
 		res.render('blogs', {
 			classify: req.session.post.classify,
 			postcode: null,
 			seeboard: markdown.makeHtml(req.session.post.postcontent),
 			articalname: req.session.post.postname,
 			time: req.session.post.time.toString().slice(0, 25),
 			title: '唐记2014',
 			user: req.session.user,
 			isedit: isedit,
 			ispreview: "true",
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 	} else
 		res.render('blogs', {
 			classify: "测试",
 			result: null,
 			seeboard: "预览出错",
 			articalname: "预览出错",
 			time: "",
 			title: '唐记2014',
 			user: req.session.user,
 			isedit: isedit,
 			ispreview: "true",
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 }

 exports.backpost = function(req, res) {
 	var currentUser = req.session.user;
 	var isedit = req.session.isedit;
 	if (req.session.post != null) {
 		var classify = req.session.post.classify;
 		var articalname = req.session.post.postname;
 		var content = req.session.post.postcontent;
 		var time = req.session.post.time;
 		res.render('eblogs', {
 			title: '唐记2014',
 			edtitle: articalname,
 			edpostcontent: content,
 			isedit: isedit,
 			ispreview: "false",
 			user: currentUser,
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 	} else
 		res.render('eblogs', {
 			title: '唐记2014',
 			edtitle: "",
 			edpostcontent: "",
 			isedit: isedit,
 			ispreview: "false",
 			user: currentUser,
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 }
 ////得到文档列表
 exports.getArticallist = function(req, res) {
 	var sendposts = [];
 	var sendclassy = ['test', 'books'];
 	var postid = "";
 	var classify = "test";
 	var articalname = "";
 	var content = "";
 	var timestamp = "";
 	var username = "";
 	username = req.session.username;
 	Post.getbynameandclassfy(username, 'test', function(err, posts) {
 		if (err) {
 			posts = [];
 		}
 		posts.forEach(function(piece) {
 			if (piece.post != "" || piece.post != null) {
 				postcode = piece.code;
 				articalname = piece.postname;
 				content = markdown.makeHtml(piece.postcontent).toString().slice(0, 125);
 				timestamp = piece.time.toString().slice(0, 25);
 				var formpost = new Posted(postcode, classify, username, articalname, content, timestamp);
 				sendposts.push(formpost);
 			}
 		});
 		res.render('listblogs', {
 			title: '唐记2014',
 			user: req.session.user,
 			classifies: sendclassy,
 			posts: sendposts,
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 	});
 }

 exports.getPostDetail = function(req, res) {
 	var currentUser = req.session.user;
 	var postcode = req.query.postcode;
 	Posted.getbypostcode(postcode, function(err, post) {
 		timestamp = post.time.toString().slice(0, 25);
 		content = markdown.makeHtml(post.postcontent);
 		res.render('blogs', {
 			title: '唐记2014',
 			user: currentUser,
 			classify: post.classify,
 			postcode: postcode,
 			articalname: post.postname,
 			time: timestamp,
 			seeboard: content,
 			ispreview: "false",
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 	});
 }
 exports.getabout = function(req, res) {
 	var currentUser = req.session.user;
 	var postcode = req.query.postcode;

 	res.render('about', {
 		title: '唐记2014',
 		user: currentUser,
 		ispreview: "false",
 		success: req.flash('success').toString(),
 		error: req.flash('error').toString()
 	});

 }

 exports.delpost = function(req, res) {
 	var currentUser = req.session.user;
 	var postcode = req.query.postcode;
 	Posted.delbypostcode(postcode, function(err, result) {
 		res.render('blogs', {
 			title: '唐记2014',
 			user: currentUser,
 			classify: "",
 			postcode: "",
 			articalname: "",
 			time: "",
 			seeboard: "",
 			ispreview: "false",
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 		if (result == "1") {
 			req.flash('success', '删除成功');
 			return res.redirect('/getArticallist');
 		}
 	});
 }

 exports.editpost = function(req, res) {
 	var currentUser = req.session.user;
 	///这里的postcode貌似没有赋值
 	var postcode = req.query.postcode;
 	req.session.postcode = postcode;
 	var isedit = "true";
 	req.session.isedit = isedit;
 	Posted.getbypostcode(postcode, function(err, post) {
 		timestamp = post.time.toString().slice(0, 25);
 		res.render('eblogs', {
 			title: '唐记2014',
 			edtitle: post.postname,
 			edpostcontent: post.postcontent,
 			classify: post.classify,
 			ispreview: "false",
 			isedit: "true",
 			user: currentUser,
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});
 	});
 }

 exports.updatepost = function(req, res) {
 	var postcode = req.session.postcode;
 	req.session.postcode = null; //过河拆桥
 	var currentUser = req.session.user;
 	var classify = "测试";
 	var articalname = req.body['edtitle'];
 	var content = req.body['edpostcontent'];
 	var time = new Date();
 	var post = new Posted(postcode, classify, currentUser.name, articalname, content, time);
 	post.update(function(err, result) {
 		if (err) {
 			req.flash('error', err);
 			return res.redirect('/');
 		}

 		if (content != null || content != "") {
 			res.render('blogs', {
 				postcode: postcode,
 				flagcode: postcode + ")-(" + post.code,
 				classify: classify,
 				seeboard: markdown.makeHtml(content),
 				articalname: articalname,
 				time: time.toString().slice(0, 25),
 				title: '唐记2014',
 				user: req.session.user,
 				ispreview: "false",
 				success: req.flash('success').toString(),
 				error: req.flash('error').toString()
 			});
 		}
 	});
 }

 exports.addtheme = function(req, res) {
 	var currentUser = req.session.user;
 	var themename = req.body['theme'];
 	var themetags = req.body['tags'];
 	var piclink = req.body['link'];
 	var theme = new Theme(currentUser, themename, themetags, piclink);
 	theme.save(function(err) {
 		if (err) {
 			req.flash('error', err);
 			return res.redirect('/');
 		}
 		res.render('index', {
 			seeboard: null,
 			title: '唐记2014',
 			postedit: '<color=\"red\">写</color>',
 			user: req.session.user,
 			success: req.flash('success').toString(),
 			error: req.flash('error').toString()
 		});

 	});
 }
