 var crypto = require('crypto');
 var mongo = require('mongodb');
 var mongoUri = process.env.MONGOLAB_URI ||
 	process.env.MONGOHQ_URL ||
 	'mongodb://localhost/microblog';

 function Posted(postcode, classify, username, postname, postcontent, time) {
 	this.code = postcode;
 	this.classify = classify;
 	this.user = username;
 	this.postname = postname;
 	this.postcontent = postcontent;
 	if (time) {
 		this.time = time;
 	} else {
 		this.time = new Date();
 	}
 };
 module.exports = Posted;

 Posted.prototype.save = function save(callback) {
 	// 存入 Mongodb 的文檔
 	var post = {
 		code: this.code,
 		classify: this.classify,
 		user: this.user,
 		postname: this.postname,
 		postcontent: this.postcontent,
 		time: this.time
 	};
 	mongo.Db.connect(mongoUri, function(err, db) {
 		db.collection('posts', function(err, collection) {
 			if (err) throw err;

 			collection.ensureIndex('user');

 			collection.insert(post, {
 				safe: true
 			}, function(err, post) {
 				// mongodb.close();
 				callback(err, post);
 			});
 		});
 	});
 };

 Posted.getbypostcode = function get(postcode, callback) {
 	mongo.Db.connect(mongoUri, function(err, db) {
 		db.collection('posts', function(err, collection) {
 			if (err) throw err;
 			//查找user属性为username的文档，如果username为null则匹配全部
 			var query = {};
 			if (postcode) {
 				query.code = postcode;
 			}

 			collection.find(query, {
 				limit: 50
 			}).sort({
 				time: -1
 			}).toArray(function(err, docs) {
 				if (err) {
 					callback(err, null);
 				}
 				callback(null, docs[0]);
 			});
 		});
 	});
 }
 Posted.delbypostcode = function get(postcode, callback) {
 	mongo.Db.connect(mongoUri, function(err, db) {
 		db.collection('posts', function(err, collection) {
 			if (err) throw err;
 			//查找user属性为username的文档，如果username为null则匹配全部
 			var query = {};
 			if (postcode) {
 				query.code = postcode;
 			}
 			collection.remove(query);
 			callback(null, "1");
 		});
 	});
 }

 Posted.prototype.update = function update(callback) {
 	// 存入 Mongodb 的文檔
 	var post = {
 		code: this.code,
 		classify: this.classify,
 		user: this.user,
 		postname: this.postname,
 		postcontent: this.postcontent,
 		time: this.time
 	};
 	mongo.Db.connect(mongoUri, function(err, db) {
 		db.collection('posts', function(err, collection) {
 			if (err) throw err;
 			//查找user属性为username的文档，如果username为null则匹配全部
 			var query={
 				code : post.code
 			};
 			collection.update(query, {
 				$set: post
 			});
 			callback(null, "1");
 		});
 	});
 }