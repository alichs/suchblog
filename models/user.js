 var mongo = require('mongodb');
 var mongoUri = process.env.MONGOLAB_URI ||
 	process.env.MONGOHQ_URL ||
 	'mongodb://localhost/microblog';

 function User(user) {
 	this.name = user.name;
 	this.password = user.password;
 };

 module.exports = User;

 User.prototype.save = function save(callback) {
 	var user = {
 		name: this.name,
 		password: this.password,
 	};

 	mongo.Db.connect(mongoUri, function(err, db) {
 		//获取users集合
 		db.collection('users', function(err, collection) {
 			if (err) throw err;
 			//为name属性添加索引
 			collection.ensureIndex('name', {
 				unique: true
 			});
 			//save
 			collection.insert(user, {
 				safe: true
 			}, function(err, user) {
 				callback(err, user);
 			});
 		});
 	});
 };

 ////类方法
 User.get = function get(username, callback) {
 	mongo.Db.connect(mongoUri, function(err, db) {
 		//获取users集合
 		db.collection('users', function(err, collection) {
 			if (err) throw err;

 			//find
 			collection.findOne({
 				name: username
 			}, function(err, doc) {
 				if (doc) {
 					var user = new User(doc);
 					callback(err, user);
 				} else {
 					callback(err, null);
 				}
 			});
 		});
 	});
 };