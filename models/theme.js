 var Guid = require('guid');
 var mongo = require('mongodb');
 var mongoUri = process.env.MONGOLAB_URI ||
 	process.env.MONGOHQ_URL ||
 	'mongodb://localhost/microblog';

 function Theme(user,themename,themetags,piclink) {
 	this.user=user;
 	this.themename = themename;
 	this.themetags = themetags;
 	this.piclink = piclink;
 };
 module.exports = Theme;


 Theme.prototype.save = function save(callback) {
	////文章id生成
	var guid=Guid.create();	//this.themeid
	var _themeid = guid.value;
 	// 存入 Mongodb 的文檔
 	var theme = {
 		user: this.user,
 		themeid: _themeid,
 		themetags: this.themetags,
 		piclink: this.piclink
 	};
 	mongo.Db.connect(mongoUri, function(err, db) {
 		db.collection('themes', function(err, collection) {
 			if (err) throw err;

 			collection.ensureIndex('user');

 			collection.insert(theme, {
 				safe: true
 			}, function(err, theme) {
 				// mongodb.close();
 				callback(err, theme);
 			});
 		});
 	});
 };


Theme.getBythemeid = function get(themeid, callback) {
 	mongo.Db.connect(mongoUri, function(err, db) {
 		db.collection('themes', function(err, collection) {
 			if (err) throw err;
 			//查找user属性为username的文档，如果username为null则匹配全部
 			var query = {};
 			if (themeid) {
 				query.themeid = themeid;
 			}
 			collection.find(query, {
 				limit: 50
 			}).sort({
 				time: -1
 			}).toArray(function(err, docs) {
 				if (err) {
 					callback(err, null);
 				}
 				callback(null, docs[0]);
 			});
 		});
 	});
 }