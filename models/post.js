 var Guid = require('guid');
 var mongo = require('mongodb');
 var mongoUri = process.env.MONGOLAB_URI ||
 	process.env.MONGOHQ_URL ||
 	'mongodb://localhost/microblog';

 function Post(classify, username, postname, postcontent, time,postcode) {
 	this.code = postcode;
 	this.classify = classify;
 	this.user = username;
 	this.postname = postname;
 	this.postcontent = postcontent;
 	if (time) {
 		this.time = time;
 	} else {
 		this.time = new Date();
 	}
 };
 module.exports = Post;

 Post.prototype.save = function save(callback) {
	////文章id生成
	var guid=Guid.create();	//this.postname
	var _postcode = guid.value;
 	// 存入 Mongodb 的文檔
 	var post = {
 		code: _postcode,
 		classify: this.classify,
 		user: this.user,
 		postname: this.postname,
 		postcontent: this.postcontent,
 		time: this.time
 	};
 	mongo.Db.connect(mongoUri, function(err, db) {
 		db.collection('posts', function(err, collection) {
 			if (err) throw err;

 			collection.ensureIndex('user');

 			collection.insert(post, {
 				safe: true
 			}, function(err, post) {
 				// mongodb.close();
 				callback(err, post);
 			});
 		});
 	});
 };

 Post.get = function get(username, callback) {
 	mongo.Db.connect(mongoUri, function(err, db) {
 		db.collection('posts', function(err, collection) {
 			if (err) throw err;
 			//查找user属性为username的文档，如果username为null则匹配全部
 			var query = {};
 			if (username) {
 				query.user = username;
 			}

 			collection.find(query, {
 				limit: 50
 			}).sort({
 				time: -1
 			}).toArray(function(err, docs) {
 				// mongodb.close();

 				if (err) {
 					callback(err, null);
 				}

 				var posts = [];

 				docs.forEach(function(doc, index) {
 					var post = new Post(doc.classify, doc.user, doc.postname, doc.postcontent, doc.time,doc.code);
 					posts.push(post);
 				});

 				callback(null, posts);
 			});
 		});
 	});
 }
 ////通过姓名和分类获取文档
 Post.getbynameandclassfy = function get(username, classfy, callback) {
 	mongo.Db.connect(mongoUri, function(err, db) {
 		db.collection('posts', function(err, collection) {
 			if (err) throw err;
 			//查找user属性为username的文档，如果username为null则匹配全部
 			var query = {};
 			if (classfy && username) {
 				query.classfy = classfy;
 				query.username = username;
 			}

 			collection.find(query, {
 				limit: 50
 			}).sort({
 				time: -1
 			}).toArray(function(err, docs) {
 				// mongodb.close();

 				if (err) {
 					callback(err, null);
 				}

 				var posts = [];

 				docs.forEach(function(doc, index) {
 					var post = new Post(doc.classify, doc.user, doc.postname, doc.postcontent, doc.time,doc.code);
 					posts.push(post);
 				});

 				callback(err, posts);
 			});
 		});
 	});
 }

