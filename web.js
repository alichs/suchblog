var express = require("express"),
	routes = require('./routes'),
	http = require('http'),
	path = require('path');
var MongoStore = require('connect-mongo')(express);
var partials = require('express-partials');
var flash = require('connect-flash');

var app = express();


app.use(express.logger());

var mongo = require('mongodb');

var mongoUri = process.env.MONGOLAB_URI ||
	process.env.MONGOHQ_URL ||
	'mongodb://localhost/microblog';

app.configure(function() {
	app.set('port', process.env.PORT || 8000);
	app.set('views', __dirname + '/views');
	app.set('view engine', 'ejs');
	app.use(partials());
	app.use(flash());
	app.use(express.favicon());
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.cookieParser());
	app.use(express.session({
		secret: "secret_meteoric",
		cookie: {
			maxAge: 60000 * 60 //60 minutes
		},
		store: new MongoStore({
			url: mongoUri
		})
	}));
	app.use(require('less-middleware')({
		src: __dirname + '/public',
		compress: true
	}));
	app.use(app.router);
	app.use(express.static(path.join(__dirname, 'public')));
});

////增加一条记录的写法
// mongo.Db.connect(mongoUri, function(err, db) {
// 	db.collection('mydocs', function(er, collection) {
// 		collection.insert({
// 			'mykey': 'myvalue'
// 		}, {
// 			safe: true
// 		}, function(er, rs) {});
// 	});
// });


app.get('/', routes.index);
app.get('/about', routes.aboutus);
app.get('/blogs', routes.getArticals);
//app.get('/post', routes.getPostDetail);
app.get('/reg', routes.reg);
app.post('/reg', routes.doReg);
app.get('/login', routes.login);
app.post('/login', routes.doLogin);
app.get('/logout', routes.logout);
app.post('/poststore',routes.post);
app.get('/eblogs',routes.showpostedit);
app.post('/postArticals',routes.postArticals);
app.post('/preview',routes.postPreview);
app.get('/backpost',routes.backpost);
app.get('/getArticallist',routes.getArticallist);
app.get('/getdetail',routes.getPostDetail);
app.get('/aboutme',routes.getabout);
app.get('/delpost',routes.delpost);
app.get('/editpost',routes.editpost);
app.post('/updatepost',routes.updatepost);
app.post('/addtheme',routes.addtheme);

app.listen(app.get('port'), function() {
	console.log("Just Listening on " + app.get('port'));
});	
